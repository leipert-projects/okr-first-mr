const _ = require('lodash');
const moment = require('moment');

const fs = require('fs');

let writeStream = fs.createWriteStream('rolling-average.csv');


const data = require('./team.json').team.reverse().map(x => {
  x.startDate = moment(x.startDate);
  return x;
});

const curr = moment('2016-01-01');

const today = moment();

const stagesToCheck = [
  "Configure",
  "Create",
  "Fulfillment",
  "Gitaly",
  "Gitter",
  "Growth",
  "Manage",
  "Monitor",
  "Plan",
  "Release",
  "Secure",
  "Serverless",
  "Verify",
];

function log(...args) {
  console.log(args.join(';'));
  writeStream.write(args.join(';') + '\n');
}

log('Date', 'Rolling average days to first MR merged', 'Candidates joined in the last 12 months');

do {

  const aYearAgo = moment(curr).subtract(1,'YEAR');

  const labbers = data.filter(x => x.mr && stagesToCheck.includes(x.stage) && x.startDate.isBetween(aYearAgo, curr, 'day', '[]'));

  const timeToMR = labbers.map(x => x.mr.mergedAfter).reduce((sum,x) => sum + x, 0) / labbers.length;

  log(curr.format('YYYY-MM-DD'), timeToMR.toFixed(2).toString(), labbers.length);

  curr.add(1, 'M');

} while (curr.isBefore(today));

writeStream.on('finish', () => {
  console.log('wrote all data to file');
});

writeStream.end();
