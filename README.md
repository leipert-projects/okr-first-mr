# Time to first merged MR after joining the company

![](2019-04-13-chart.png)

This repository contains the code to do a rolling average of the time to first MR being merged.

Results can be found in the latest CI job: https://gitlab.com/leipert-projects/okr-first-mr/-/jobs/artifacts/master/browse?job=build_page


### Methodology

`getData.js`:

1. Download `team.yml` from www-gitlab-com repo
2. Get member id of each team member and start date
3. Get all merged MRs:
    - that this member id authored
    - ascending by creation date
    - Filter for MRs only touching `/gitlab-org/`
    - Get the MR that has the smallest `merged_at - startDate`
4. Write mr info, gitlab handle, startDate, stage, role and id into `team.json`

Caveats:
  - team members that left the company are not considered
  - team members could have faulty gitlab handles in the team.yml
  - team members could have wrong stage info in the team.tml
  - This doesn't consider dev.gitlab.org

`analysis.js`

1. Read team.json from above
2. Start iterating for each month from 2016-01-01
3. Get all employees that:
  - started between the current date and 12 months prior
  - have an actual MR merged
  - belong to one of the [development stages](https://gitlab.com/leipert-projects/okr-first-mr/blob/983164740c5bb745634e16ca46bbae2875d63188/analysis.js#L18-32)
4. Sum up their timeToFirstMr and divide by number of people
5. Write results into rolling-average.csv

