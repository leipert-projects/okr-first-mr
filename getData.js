const fs = require("fs");

const _ = require("lodash");
const Promise = require("bluebird");

const axios = require("axios");
const moment = require("moment");
const yaml = require("yaml-js");

const TEAM_MAP_VERSION = 1;

let cached = [];

try {
  const previous = JSON.parse(fs.readFileSync("./team.json", "utf8"));
  if (_.get(previous, "version") === TEAM_MAP_VERSION) {
    cached = previous.team;
  }
} catch (e) {
  console.log("Could not load existing reverse coded team.json");
}

function compareMembers(a, b) {
  return a.gitlab === b.gitlab;
}

const stageMap = {
  "Secure FE Team": "Secure",
  "Secure BE Team": "Secure",
  "Create FE Team": "Create",
  "Create BE Team": "Create",
  "Plan FE Team": "Plan",
  "Plan BE Team": "Plan",
  "Release BE Team": "Release",
  "Release FE Team": "Release",
  "Manage FE Team": "Manage",
  "Manage BE Team": "Manage",
  "Monitor BE Team": "Monitor",
  "Monitor FE Team": "Monitor",
  "Verify BE Team": "Verify",
  "Verify FE Team": "Verify",
  "Verify Team": "Verify",
  "Configure BE Team": "Configure",
  "Configure FE Team": "Configure",
  "Growth Sub-department": "Growth",
  "Serverless FE Team": "Serverless",
  "Serverless BE Team": "Serverless",
  "Fulfillment BE Team": "Fulfillment",
  "Fulfillment FE Team": "Fulfillment",
  "Gitaly Team": "Gitaly",
  "Gitter Team": "Gitter",
  //
  "Security Department": "Security",
  Meltano: "Meltano",
  "UX Department": "UX",
  UX: "UX",
  "Enablement Sub-department": "Enablement",
  "Quality Department": "Quality",
  "Support Department": "Support",
  "Infrastructure Department": "Infrastructure"
};

function getStage(departments) {
  for (let [key, value] of Object.entries(stageMap)) {
    if (departments.includes(key)) {
      return value;
    }
  }

  return "Other";
}

function getUserId(gitlab) {
  if (cached[gitlab] && cached[gitlab].id) {
    console.log(`Cached result for ${gitlab}`);
    return cached[gitlab].id;
  }

  return axios({
    method: "get",
    url: `https://gitlab.com/api/v4/users?username`,
    params: {
      username: gitlab
    }
  }).then(response => {
    const id = _.get(response, "data.[0].id", false);

    if (!id) {
      console.warn(`No id found for ${gitlab}`);
    }

    return id;
  });
}

const sortMrs = (mrs, startDate) => {
  return _.chain(mrs)
    .map(({ web_url, merged_at, created_at }) => {
      return {
        webUrl: web_url,
        mergedAt: merged_at,
        mergedAfter: moment(merged_at).diff(moment(startDate), "d"),
        createAt: created_at,
        createdAfter: moment(created_at).diff(moment(startDate), "d")
      };
    })
    .filter(mr => mr.webUrl.includes("/gitlab-org/"))
    .orderBy(["mergedAfter", "createdAfter"], ["asc", "asc"])
    .value();
};

function getFirstMergedMr({ gitlab, id, startDate }) {
  if (cached[gitlab] && cached[gitlab].firstMr) {
    console.log(`Cached result for ${gitlab}`);
    return cached[gitlab].firstMr;
  }

  return axios({
    method: "get",
    url: "https://gitlab.com/api/v4/merge_requests",
    params: {
      author_id: id,
      scope: "all",
      order_by: "created_at",
      sort: "asc",
      state: "merged",
      created_after: startDate
    }
  })
    .then(response => {
      const MRs = sortMrs(_.get(response, "data", []), startDate);
      return _.first(MRs);
    });
}

function mapMember(member) {
  const stage = getStage(member.departments);

  console.log(
    `Getting data for ${member.name} - ${stage} (${member.start_date})`
  );

  return Promise.props({
    gitlab: member.gitlab,
    startDate: member.start_date,
    stage,
    role: member.role,
    id: getUserId(member.gitlab)
  }).then(data => {
    const mr = data.id ? getFirstMergedMr(data) : false;

    return Promise.props({
      mr,
      ...data
    });
  });
}

function bail(e) {
  console.warn(`Error creating the team page map:\n\t${e}`);
  process.exit(1);
}

function isEngineer(member) {
  const { departments } = member;

  return departments.some(d => d === "Engineering Function");
}

// Get document, or throw exception on error
try {
  const doc = yaml.load(fs.readFileSync("./team.yml", "utf8"));

  const members = _.chain(doc)
    .filter(
      member =>
        member.type !== "vacancy" &&
        member.gitlab &&
        isEngineer(member) &&
        member.slug !== "open-roles" &&
        member.picture !== "../gitlab-logo-extra-whitespace.png"
    )
    .orderBy(["start_date"], ["desc"])
    .value();

  const tooMuch = _.differenceWith(cached, members, compareMembers);

  if (tooMuch.length > 0) {
    console.log(`Going to remove ${tooMuch.map(x => x.gitlab).join(", ")}`);
    cached = _.difference(cached, tooMuch);
  }

  cached = _.keyBy(cached, "gitlab");

  Promise.map(members, mapMember, { concurrency: 1 })
    .then(team => {
      console.log("\nFound a location for all team members");
      const result = { version: TEAM_MAP_VERSION, team };
      return fs.writeFileSync("./team.json", JSON.stringify(result));
    })
    .catch(bail);
} catch (e) {
  bail(e);
}
