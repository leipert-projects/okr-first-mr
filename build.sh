#!/usr/bin/env bash
# Use the unofficial bash strict mode: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail; export FS=$'\n\t'

# Download cached team.json and new team.yml
curl --fail --location --data "job=build_page" --remote-name --get \
  'https://gitlab.com/leipert-projects/okr-first-mr/-/jobs/artifacts/master/raw/public/team.json' \
  || echo "No cached team.json available"
curl --fail --remote-name --location https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/team.yml

rm rolling-average.csv

# Update team.json with data from the team.yml
yarn
node ./getData.js
node ./analysis.js

# Copy assets into public folder
mkdir -p public/
cp team.json public/
cp rolling-average.csv public/

# GZIP assets
find public -type f | xargs gzip -f -k
